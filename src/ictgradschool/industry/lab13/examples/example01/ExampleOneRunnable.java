package ictgradschool.industry.lab13.examples.example01;

/**
 * A simple Runnable that will continually increment the counter until its thread is interrupted.
 */
public class ExampleOneRunnable implements Runnable {

    private long counter = 0;

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) { //This is how the runnable object calls its thread
            counter++;
        }
    }

    public long getCounter() {
        return counter;
    }
}
